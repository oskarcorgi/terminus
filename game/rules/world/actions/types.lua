-- Copyright (c) 2021 Trevor Redfern
--
-- This software is released under the MIT License.
-- https://opensource.org/licenses/MIT

return {
  ENTITY_ADD = "ENTITY_ADD",
  ENTITY_REMOVE = "ENTITY_REMOVE"
}