-- Copyright (c) 2021 Trevor Redfern
--
-- This software is released under the MIT License.
-- https://opensource.org/licenses/MIT

return function()
  return function()
    require "assets.characters.skills"
    require "assets.characters.names"
    require "assets.items"
    require "assets.maps.terrains"
  end
end