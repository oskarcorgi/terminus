-- Copyright (c) 2021 Trevor Redfern
--
-- This software is released under the MIT License.
-- https://opensource.org/licenses/MIT

return {
  ADD = "MAP_ADD",
  CREATE = "MAP_CREATE",
  SET_TERRAIN = "MAP_SET_TERRAIN",
}