-- Copyright (c) 2021 Trevor Redfern
--
-- This software is released under the MIT License.
-- https://opensource.org/licenses/MIT

describe("game.rules.map.generators.dungeon", function()
  local dungeon = require "game.rules.map.generators.dungeon"
  local Outline = require "game.rules.map.outline"

  it("can create a node for the tree", function()
    local node = dungeon.create_node(1, 1, 100, 100)
    assert.equals(1, node.x)
    assert.equals(1, node.y)
    assert.equals(100, node.width)
    assert.equals(100, node.height)
  end)

  it("divides a node into 2 sub-nodes on the x-axis", function()
    local node = dungeon.create_node(1, 1, 100, 100)
    dungeon.divide(node, 1, 1)
    assert.equals(1, node.left.x)
    assert.equals(1, node.left.y)
    assert.equals(node.right.x, node.left.x + node.left.width)
    assert.equals(100, node.left.height)


    assert.equals(node.left.x + node.left.width, node.right.x)
    assert.equals(1, node.right.y)
    assert.equals(100 - node.right.x + 1, node.right.width)
    assert.equals(100, node.right.height)
  end)

  it("will divide on y-axis if height is greater than width", function()
    local node = dungeon.create_node(1, 1, 50, 100)
    dungeon.divide(node, 1, 1)
    assert.equals(1, node.left.x)
    assert.equals(1, node.left.y)
    assert.equals(50, node.left.width)
    assert.equals(node.right.y, node.left.y + node.left.height)


    assert.equals(1, node.right.x)
    assert.equals(node.left.y + node.left.height, node.right.y)
    assert.equals(50, node.right.width)
    assert.equals(100 - node.right.y + 1, node.right.height)
  end)

  it("does not divide a node if not at least 6 width and height", function()
    local node = dungeon.create_node(1, 1, 5, 5)
    dungeon.divide(node, 1, 3)
    assert.is_nil(node.left)
    assert.is_nil(node.right)
  end)

  it("subdivides the tree to a certain level", function()
    local node = dungeon.create_node(1, 1, 100, 100)
    dungeon.divide(node, 1, 4)

    assert.not_nil(node.left.left.left)
  end)

  it("builds a room within the boundaries of the node", function()
    local node = dungeon.create_node(15, 18, 23, 27)
    local outline = Outline:new(100, 100)
    dungeon.create_rooms(node, outline)

    assert.not_nil(node.room)
    assert.is_true(node.room.x >= node.x)
    assert.is_true(node.room.y >= node.y)
    assert.is_true(node.room.width <= node.width)
    assert.is_true(node.room.height <= node.height)

    assert.is_true(node.room.x + node.room.width <= node.x + node.width)
    assert.is_true(node.room.y + node.room.height <= node.y + node.height)
  end)

  it("generates an outline and tilemap for the dungeon", function()
    local outline, tileMap = dungeon.generate(50, 50, 1)
    assert.not_nil(outline)
    assert.not_nil(tileMap)

    assert.greater_than(0, #outline.rooms)
    assert.greater_than(0, #outline.corridors)
    assert.greater_than(0, tileMap.width)
    assert.greater_than(0, tileMap.height)
  end)

  it("can create a multi-level dungeon", function()
    local outline, tileMap = dungeon.generate(50, 50, 10)
    assert.equals(10, outline.levels)
    assert.equals(10, tileMap.levels)
  end)
end)