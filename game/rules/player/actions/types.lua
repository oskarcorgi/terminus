-- Copyright (c) 2021 Trevor Redfern
--
-- This software is released under the MIT License.
-- https://opensource.org/licenses/MIT

return {
  LADDER_DOWN = "PLAYER_LADDER_DOWN",
  LADDER_UP = "PLAYER_LADDER_UP",
  OPEN_DOOR = "PLAYER_OPEN_DOOR"
}